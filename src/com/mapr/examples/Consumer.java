package com.mapr.examples;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;
import java.util.Random;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * This program reads messages from two topics. Messages on "fast-messages" are analyzed
 * to estimate latency (assuming clock synchronization between producer and consumer).
 * <p/>
 * Whenever a message is received on "slow-messages", the stats are dumped.
 */
public class Consumer {
    public static void main(String[] args) throws IOException {
        // set up house-keeping
        ObjectMapper mapper = new ObjectMapper();

        // and the consumer
        KafkaConsumer<String, String> consumer;
        final String TOPIC ;
        
        try (FileInputStream in = new FileInputStream(new File("./resources/consumer.props"))) {
        	
            Properties properties = new Properties();
            properties.load(in);
            if (properties.getProperty("group.id") == null) {
                properties.setProperty("group.id", "group-" + new Random().nextInt(100000));
            }
            consumer = new KafkaConsumer<>(properties);
            TOPIC = (String)properties.get("topic");
        }
        
        consumer.subscribe(Arrays.asList(TOPIC));
        int timeouts = 0;
        //noinspection InfiniteLoopStatement
        while (true) {
            // read records with a short timeout. If we time out, we don't really care.
            ConsumerRecords<String, String> records = consumer.poll(200);
            if (records.count() == 0) {
                timeouts++;
            } else {
                System.out.printf("Got %d records after %d timeouts\n", records.count(), timeouts);
                timeouts = 0;
            }
            for (ConsumerRecord<String, String> record : records) {
                if (record.topic().equals(TOPIC)){
                		try {
                				
                			System.out.println("Se esta recibiendo: "+record.value());	
                			
                		}
                		catch (Exception e)
                		{
                			System.out.printf("%s", e.getStackTrace());
                			
                		}
                        
                }
            }
        }
    }
}