package com.mapr.examples;

import java.io.IOException;

import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.visualizers.StatGraphVisualizer;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

/**
 * Pick whether we want to run as producer or consumer. This lets us
 * have a single executable as a build target.
 */
public class Run extends StatGraphVisualizer implements JavaSamplerClient {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static transient Logger log = LoggingManager.getLoggerForClass();
	
	@Override
	public Arguments getDefaultParameters() {
		log.info("Ejecutando Kafka Producer - No se ha inicializado ningun parametro por defecto");
		return null;
	}

	@Override
	public void setupTest(JavaSamplerContext arg0) {
		log.info("Ejecutando Kafka Producer - No se requiere configuracion");
	   
	}

	@Override
	public void teardownTest(JavaSamplerContext arg0) {
		log.info("Ejecutando Kafka Producer - No se requiere configuracion");
	}

	@Override
	public SampleResult runTest(JavaSamplerContext arg0) {
		SampleResult sampleResult = new SampleResult();
		
		log.info("Ejecutando Kafka Producer - Se ha construido el objeto SampleResult");
		try {
			sampleResult.sampleStart();
			Producer.main(null);
			log.info("Ejecutando Kafka Producer - Se ejecuto correctamente la peticion");
			sampleResult.sampleEnd();
		    sampleResult.setSuccessful(true);
		    sampleResult.setResponseCodeOK();
		    sampleResult.setResponseMessageOK();

		} catch (IOException e) {
			sampleResult.sampleEnd();
			log.error("Ejecutando Kafka Producer - Se ha presentado un error " + e.getMessage());
		    sampleResult.setSuccessful(false);
		    sampleResult.setResponseMessage(e.getMessage());;
		    
		}
		catch (Exception e) {
			sampleResult.sampleEnd();
			log.error("Ejecutando Kafka Producer - Se ha presentado un error " + e.getMessage());
		    sampleResult.setSuccessful(false);
		    sampleResult.setResponseMessage(e.getMessage());;
		    
		}
		finally {
			log.info("Ejecutando Kafka Producer - Peticion finalizada");
			
		}
		
		return sampleResult;
	}
}
