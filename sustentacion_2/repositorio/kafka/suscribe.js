/**
 * Created by OscarVargas on 2/27/16.
 */
var mqtt    = require('mqtt');
var client  = mqtt.connect('mqtt://localhost');
var mappingEnum  = require('./configureKafkaMosco.js').Mapping;


var Kafka = require('no-kafka');
var producer = new Kafka.Producer();


producer.init().then(function(){
        return producer.send({
            topic: 'temperature',
            partition: 0,
            message: {
                value: '1'
            }
        });
    });


client.on('connect', function () {

    client.subscribe('celsius');
    client.subscribe('fahrenheit');
    client.subscribe('test');


    client.on('message', function (topic, message) {
        console.log(topic +": "+message.toString());

        producer.init().then(function(){
            return producer.send({
                //bug on producer when use a enum
                connectionString: '127.0.0.1:9092',

                topic: topic,//mappingEnum[topic.toString()],
                partition: 0,
                message: {
                    value: message.toString()
                }
            });
        });



    });
});


