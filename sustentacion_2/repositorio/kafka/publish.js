/**
 * Created by OscarVargas on 2/27/16.
 */
var mqtt    = require('mqtt');
var client  = mqtt.connect('mqtt://localhost');

client.on('connect', function () {
    client.subscribe('illumination');
    client.publish('illumination', 'Hello mqtt');
});

client.on('message', function (topic, message) {
    // message is Buffer
    console.log(message.toString());
    client.end();
});

client.on('connect', function () {

    client.publish('illumination', 'Hello!', {retain: false, qa: 1});
    client.end();
});